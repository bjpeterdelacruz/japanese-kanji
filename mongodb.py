import json
from os import walk
import pymongo
import ssl


with open('mongo_db_credentials.txt', 'r') as f:
    username, password, url = f.readline().split(',')

client = pymongo.MongoClient(f"mongodb+srv://{username}:{password}@"
                             f"{url}?retryWrites=true&w=majority", ssl_cert_reqs=ssl.CERT_NONE)
db = client.test
kanjis = db['kanjis']


def get_all():
    return kanjis.find({}, {'_id': 0})


def get_kanji(search_by: str, search_term: str):
    return kanjis.find_one({search_by: search_term}, {'_id': 0})


if __name__ == '__main__':
    with open('kanjiapi_full.json', 'r') as f:
        data = json.load(f)

    for obj in data['kanjis']:
        with open(f'json/{obj}.json', 'w', encoding="utf-8") as f:
            kanji_json = json.dumps(data['kanjis'][obj], ensure_ascii=False).encode("utf-8")
            f.write(kanji_json.decode())

    arr = []
    for (_, _, filenames) in walk('json'):
        for filename in filenames:
            with open(f'json/{filename}', 'r') as f:
                kanji = f.read()
                arr.append(json.loads(kanji))

    kanjis.insert_many(arr)
