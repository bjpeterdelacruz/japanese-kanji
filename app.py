import mongodb
from random import randint, seed
from flask import Flask, render_template, request, escape

app = Flask(__name__)

all_kanji = [kanji for kanji in mongodb.get_all()]

seed()


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/kanji')
def get_kanji():
    return mongodb.get_kanji(escape(request.args.get('searchBy')),
                             escape(request.args.get('value')))


@app.route('/random-kanji')
def get_random_kanji():
    return all_kanji[randint(0, len(all_kanji) - 1)]


if __name__ == '__main__':
    app.run()
